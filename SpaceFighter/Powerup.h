#pragma once

#include "GameObject.h"

class Powerup : public GameObject
{

public:

	// Created this here to declare this special-member function in the class.
	Powerup();
	virtual ~Powerup() { }

	// <---Copied from "BioEnemyShip.h"
	void SetTexture(Texture *pTexture) { m_pTexture = pTexture; }

	// Copied from "Ship.h"
	virtual void Update(const GameTime *pGameTime);

	// This draws the powerup sprite
	virtual void Draw(SpriteBatch *pSpriteBatch);

	// Copied from "Ship.h" but gave it only 1 parameter instead of 2.
	virtual void Initialize(const Vector2 position, const double delaySeconds);

	// We don't have a Fire() { } funtion (and we shouldn't need one)

	// This gives the user the buff (EnemyShip.h uses "Hit()")
	virtual void PowerupGet();

	// Probably got from EnemyShip.h
	virtual std::string ToString() const { return "Powerup"; }

	// THIS SETS THE COLLISION TYPE TO A 'POWERUP' TYPE 
	virtual CollisionType GetCollisionType() const { return CollisionType::POWERUP; }

	// <---Copied over from Ship.h
	virtual float GetSpeed() const { return m_speed; }

	// <---Copied over from Ship.h
	virtual void SetSpeed(const float speed) { m_speed = speed; }


	// <------------------------All the new stuff we added to make Powerup work--------------------->

	// <---This checks if the user currently has the buff applied because they got the powerup already ------->
	virtual bool IsPowerupActive() { return m_isPowerupActive; }
	
	// <---This works with the variable that sets the state of the Active Powerup to true
	virtual void SetPowerupActive(bool powerupActive) { m_isPowerupActive = powerupActive; }
	
	// <---Sets and Gets our Cooldown time for the powerup.
	virtual void SetCooldown(float activeTime) { m_cooldown = activeTime; }
	virtual float GetCooldown() { return m_cooldown; }
	

private:

	bool m_isPowerupActive;
	
	float m_speed;

	float m_cooldown;
	
	float m_activationSeconds;
	
	double m_delaySeconds;

	Texture *m_pTexture;
};
