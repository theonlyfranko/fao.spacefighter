
#include "PlayerShip.h"

// <--- Frank Comment on Methods 1 --->
void PlayerShip::LoadContent(ResourceManager *pResourceManager)
{
	// keeps the ship on the screen (and based off of comments below it looks like it keeps the ship 4 pixels away from the edge of the screen).
	ConfineToScreen();
	// This sets an amount of time between when the user presses a button and when that button's actions are activated.
	SetResponsiveness(0.1);

	// This imports a picture of the ship for the user to see.
	m_pTexture = pResourceManager->Load<Texture>("Textures\\PlayerShip.png");
	// This function sets the ship in the middle of the screen to start the level.
	SetPosition(Game::GetScreenCenter() + Vector2::UNIT_Y * 300);
}


 void PlayerShip::Update(const GameTime *pGameTime)
 {
	 // Get the velocity for the direction that the player is trying to go.
	 Vector2 targetVelocity = m_desiredDirection * GetSpeed() * pGameTime->GetTimeElapsed();
	 // We can't go from 0-100 mph instantly! This line interpolates the velocity for us.
	 m_velocity = Vector2::Lerp(m_velocity, targetVelocity, GetResponsiveness());
	 // Move that direction
	 TranslatePosition(m_velocity);

	 if (m_isConfinedToScreen)
	 {
		 const int PADDING = 4; // keep the ship 4 pixels from the edge of the screen
		 const int TOP = PADDING;
		 const int LEFT = PADDING;
		 const int RIGHT = Game::GetScreenWidth() - PADDING;
		 const int BOTTOM = Game::GetScreenHeight() - PADDING;

		 Vector2 *pPosition = &GetPosition(); // current position (middle of the ship)
		 if (pPosition->X - GetHalfDimensions().X < LEFT) // are we past the left edge?
		 {
			 // move the ship to the left edge of the screen (keep Y the same)
			 SetPosition(LEFT + GetHalfDimensions().X, pPosition->Y);
			 m_velocity.X = 0; // remove any velocity that could potentially
							   // keep the ship pinned against the edge
		 }
		 if (pPosition->X + GetHalfDimensions().X > RIGHT) // right edge?
		 {
			 SetPosition(RIGHT - GetHalfDimensions().X, pPosition->Y);
			 m_velocity.X = 0;
		 }
		 if (pPosition->Y - GetHalfDimensions().Y < TOP) // top edge?
		 {
			 SetPosition(pPosition->X, TOP + GetHalfDimensions().Y);
			 m_velocity.Y = 0;
		 }
		 if (pPosition->Y + GetHalfDimensions().Y > BOTTOM) // bottom edge?
		 {
			 SetPosition(pPosition->X, BOTTOM - GetHalfDimensions().Y);
			 m_velocity.Y = 0;
		 }
	 }

	 // do any updates that a normal ship would do.
	 // (fire weapons, collide with objects, etc.)
	 Ship::Update(pGameTime);
 }

// <--- Frank Method Comments 2 --->
 void PlayerShip::Draw(SpriteBatch *pSpriteBatch)
 {
 	// if the ship hasn't been destroyed yet, perform the following function:
	 if (IsActive())
	 {
	 	// This gets the position the user is in and puts a white colored ship in that position on the screen.
		 pSpriteBatch->Draw(m_pTexture, GetPosition(), Color::White, m_pTexture->GetCenter());
	 }
 }

 void PlayerShip::HandleInput(const InputState *pInput)
 {
	 if (IsActive())
	 {
		 Vector2 direction = Vector2::ZERO;
		 if (pInput->IsKeyDown(Key::DOWN)) direction.Y++;
		 if (pInput->IsKeyDown(Key::UP)) direction.Y--;
		 if (pInput->IsKeyDown(Key::RIGHT)) direction.X++;
		 if (pInput->IsKeyDown(Key::LEFT)) direction.X--;

		 // Normalize the direction
		 if (direction.X != 0 && direction.Y != 0)
		 {
			 direction *= Math::NORMALIZE_PI_OVER4;
		 }

		 //TriggerType type = TriggerType::NONE;
		 //if (pInput->IsKeyDown(Key::F)) type |= TriggerType::PRIMARY;
		 //if (pInput->IsKeyDown(Key::D)) type |= TriggerType::SECONDARY;
		 //if (pInput->IsKeyDown(Key::S)) type |= TriggerType::SPECIAL;

		 GamePadState *pState = &pInput->GetGamePadState(0);
		 if (pState->IsConnected)
		 {
			 // gamepad overrides keyboard input
			 Vector2 thumbstick = pState->Thumbsticks.Left;
			 if (thumbstick.LengthSquared() < 0.08f) thumbstick = Vector2::ZERO;
			 direction = thumbstick;

			 //type = TriggerType::NONE;
			 //if (pState->Triggers.Right > 0.5f) type |= TriggerType::PRIMARY;
			 //if (pState->Triggers.Left > 0.5f) type |= TriggerType::SECONDARY;
			 //if (pState->IsButtonDown(Button::Y)) type |= TriggerType::SPECIAL;
		 }


		 SetDesiredDirection(direction);
		 //if (type != TriggerType::NONE) FireWeapons(type);

		 if (pInput->IsKeyDown(Key::SPACE)) FireWeapons(TriggerType::PRIMARY);
	 }
 }

// <--- Frank Method Comments 3 --->
 void PlayerShip::Initialize(Level *pLevel, Vector2 &startPosition)
 {
 	 // gets the level that the player is at and puts them in the correct starting position based off of this level information.
	 SetPosition(startPosition);
 }


 Vector2 PlayerShip::GetHalfDimensions() const
 {
	 return m_pTexture->GetCenter();
 }

 void PlayerShip::SetResponsiveness(const float responsiveness)
 {
	 m_responsiveness = Math::Clamp(0, 1, responsiveness);
 }
