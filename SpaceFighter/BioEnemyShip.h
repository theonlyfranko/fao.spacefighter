
#pragma once

#include "EnemyShip.h"

class BioEnemyShip : public EnemyShip
{

public:

	BioEnemyShip();
	virtual ~BioEnemyShip() { }

	void SetTexture(Texture *pTexture) { m_pTexture = pTexture; }

	virtual void Update(const GameTime *pGameTime); // EX: Pure Virtual #1

	virtual void Draw(SpriteBatch *pSpriteBatch); // EX: Pure Virtual #2


private:

	Texture *m_pTexture;

};