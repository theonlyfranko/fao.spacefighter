#include "Powerup.h"

Powerup::Powerup()
{
	SetSpeed(200);
	SetCollisionRadius(20);
	Activate();
}

void Powerup::Update(const GameTime *pGameTime)
{
	if (m_cooldown != 0)
	{
		m_cooldown -= pGameTime->GetTimeElapsed();
	}
	else SetPowerupActive(false);

	if (IsActive())
	{
		float x = sin(pGameTime->GetTotalTime() * Math::PI + GetIndex());
		x *= GetSpeed() * pGameTime->GetTimeElapsed() * 1.4f;
		TranslatePosition(x, GetSpeed() * pGameTime->GetTimeElapsed());

		if (m_delaySeconds > 0)
		{
			m_delaySeconds -= pGameTime->GetTimeElapsed();
			
			if (m_delaySeconds <= 0)
			{
				GameObject::Activate();
			}
		}
		
		if (IsActive())
		{
			m_activationSeconds += pGameTime->GetTimeElapsed();
			if (m_activationSeconds > 2 && !IsOnScreen())
			{
				Deactivate();
			}
		}
		
		GameObject::Update(pGameTime);
	}
}


void Powerup::Draw(SpriteBatch *pSpriteBatch)
{
	if (IsActive())
	{
		pSpriteBatch->Draw(m_pTexture, GetPosition(), Color::White, m_pTexture->GetCenter(), Vector2::ONE, 1);
		GetPosition().Display();
	}
}


void Powerup::Initialize(const Vector2 position, const double delaySeconds)
{
	SetPosition(position);
	m_delaySeconds = delaySeconds;
}


void Powerup::PowerupGet()
{
	SetCooldown(12);
	SetPowerupActive(true);
}


